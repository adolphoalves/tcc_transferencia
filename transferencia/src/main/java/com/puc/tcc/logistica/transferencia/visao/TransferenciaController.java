package com.puc.tcc.logistica.transferencia.visao;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.puc.tcc.logistica.transferencia.dominio.Transferencia;
import com.puc.tcc.logistica.transferencia.servicos.ServicosDeTransferencia;
 
@RestController
@RequestMapping("/transferencia")
public class TransferenciaController {

	@Autowired
	private ServicosDeTransferencia servicosDeTransferencia;
	
	
	@GetMapping("/todas")
	ResponseEntity<List<Transferencia>> all() {
		return ResponseEntity.status(HttpStatus.OK).body(servicosDeTransferencia.getAll());
	}
	
	
}
