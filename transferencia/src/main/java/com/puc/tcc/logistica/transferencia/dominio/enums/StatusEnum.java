package com.puc.tcc.logistica.transferencia.dominio.enums;

public enum StatusEnum {
	
    NOVO(1),TRANSFERIDO(2);
 
    public int codigo;
    
    StatusEnum(int codigo) {
    	this.codigo = codigo;
    }

}
