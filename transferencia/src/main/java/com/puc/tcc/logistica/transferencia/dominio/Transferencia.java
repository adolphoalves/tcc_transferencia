package com.puc.tcc.logistica.transferencia.dominio;

import java.util.Date;

public class Transferencia {
	
	private long codigoPedido;
	private Date data;
	private float custo;
	private String descricaoTrecho;
	
	public long getCodigoPedido() {
		return codigoPedido;
	}
	public void setCodigoPedido(long codigoPedido) {
		this.codigoPedido = codigoPedido;
	}
	public Date getData() {
		return data;
	}
	public void setData(Date data) {
		this.data = data;
	}
	public float getCusto() {
		return custo;
	}
	public void setCusto(float custo) {
		this.custo = custo;
	}
	public String getDescricaoTrecho() {
		return descricaoTrecho;
	}
	public void setDescricaoTrecho(String descricaoTrecho) {
		this.descricaoTrecho = descricaoTrecho;
	}

}
