package com.puc.tcc.logistica.transferencia.webservices.correio;

import javax.xml.bind.JAXBElement;
import javax.xml.namespace.QName;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Component;

import com.puc.tcc.logistica.transferencia.webservices.correio.servico.ConsultaCEP;
import com.puc.tcc.logistica.transferencia.webservices.correio.servico.ConsultaCEPResponse;
import com.puc.tcc.logistica.transferencia.webservices.correio.servico.EnderecoERP;

@Component
public class CorreioWebservice {

	@Autowired
	SOAPConnector soapConnector;
	
	public EnderecoERP buscaCep(String cep) {
		
		ConsultaCEP request = new ConsultaCEP();
		request.setCep(String.valueOf(cep));
		QName qName = new QName("http://cliente.bean.master.sigep.bsb.correios.com.br/", "consultaCEP");
        JAXBElement<ConsultaCEP> root = (JAXBElement<ConsultaCEP>) new JAXBElement<>(qName, ConsultaCEP.class, request);
        @SuppressWarnings({"rawtypes" })
		JAXBElement jaxResponse = (JAXBElement) soapConnector.callWebService(
				"https://apps.correios.com.br/SigepMasterJPA/AtendeClienteService/AtendeCliente", root);
        ConsultaCEPResponse response = (ConsultaCEPResponse)jaxResponse.getValue();
        if(response != null && response.getReturn() != null) {
        	EnderecoERP endereco = response.getReturn();
        	return endereco;
        } else {
        	return null;
        }
	}

}
