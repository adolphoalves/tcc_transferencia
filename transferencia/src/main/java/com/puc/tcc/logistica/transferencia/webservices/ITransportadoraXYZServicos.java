package com.puc.tcc.logistica.transferencia.webservices;

import java.util.List;

import com.puc.tcc.logistica.transferencia.dominio.Pedido;
import com.puc.tcc.logistica.transferencia.dominio.Transferencia;

public interface ITransportadoraXYZServicos {
	
	Transferencia transfer(Pedido pedido);
	
	List<Transferencia> getAll();

}