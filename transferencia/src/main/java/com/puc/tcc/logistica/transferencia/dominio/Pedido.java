package com.puc.tcc.logistica.transferencia.dominio;

import java.util.Date;

import com.puc.tcc.logistica.transferencia.dominio.enums.StatusEnum;

public class Pedido {
	
	private long id;
	private long idCliente;
	private String descricaoCarga;
	private Endereco enderecoOrigem;
	private Endereco enderecoDestino;
	private boolean temCuidadosEspeciais;
	private float valor;
	private StatusEnum status;
	private int alturaCaixa;
	private int larguraCaixa;
	private int profundidadeCaixa;
	private float peso;
	private Date dataPedido;
	private Date dataExpedicao;
	private Date dataEntrega;
	
	
	public long getId() {
		return id;
	}
	public void setId(long id) {
		this.id = id;
	}
	public long getIdCliente() {
		return idCliente;
	}
	public void setIdCliente(long idCliente) {
		this.idCliente = idCliente;
	}
	public String getDescricaoCarga() {
		return descricaoCarga;
	}
	public void setDescricaoCarga(String descricaoCarga) {
		this.descricaoCarga = descricaoCarga;
	}
	public Endereco getEnderecoOrigem() {
		return enderecoOrigem;
	}
	public void setEnderecoOrigem(Endereco enderecoOrigem) {
		this.enderecoOrigem = enderecoOrigem;
	}
	public Endereco getEnderecoDestino() {
		return enderecoDestino;
	}
	public void setEnderecoDestino(Endereco enderecoDestino) {
		this.enderecoDestino = enderecoDestino;
	}
	public boolean isTemCuidadosEspeciais() {
		return temCuidadosEspeciais;
	}
	public void setTemCuidadosEspeciais(boolean temCuidadosEspeciais) {
		this.temCuidadosEspeciais = temCuidadosEspeciais;
	}
	public float getValor() {
		return valor;
	}
	public void setValor(float valor) {
		this.valor = valor;
	}
	public StatusEnum getStatus() {
		return status;
	}
	public void setStatus(StatusEnum status) {
		this.status = status;
	}
	public int getAlturaCaixa() {
		return alturaCaixa;
	}
	public void setAlturaCaixa(int alturaCaixa) {
		this.alturaCaixa = alturaCaixa;
	}
	public int getLarguraCaixa() {
		return larguraCaixa;
	}
	public void setLarguraCaixa(int larguraCaixa) {
		this.larguraCaixa = larguraCaixa;
	}
	public int getProfundidadeCaixa() {
		return profundidadeCaixa;
	}
	public void setProfundidadeCaixa(int profundidadeCaixa) {
		this.profundidadeCaixa = profundidadeCaixa;
	}
	public float getPeso() {
		return peso;
	}
	public void setPeso(float peso) {
		this.peso = peso;
	}
	public Date getDataPedido() {
		return dataPedido;
	}
	public void setDataPedido(Date dataPedido) {
		this.dataPedido = dataPedido;
	}
	public Date getDataExpedicao() {
		return dataExpedicao;
	}
	public void setDataExpedicao(Date dataExpedicao) {
		this.dataExpedicao = dataExpedicao;
	}
	public Date getDataEntrega() {
		return dataEntrega;
	}
	public void setDataEntrega(Date dataEntrega) {
		this.dataEntrega = dataEntrega;
	}
}
