package com.puc.tcc.logistica.transferencia.servicos;

import java.util.List;

import org.apache.kafka.clients.consumer.ConsumerRecord;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.kafka.core.KafkaTemplate;
import org.springframework.stereotype.Service;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.puc.tcc.logistica.transferencia.dominio.Pedido;
import com.puc.tcc.logistica.transferencia.dominio.Transferencia;
import com.puc.tcc.logistica.transferencia.webservices.ITransportadoraXYZServicos;

@Service
public class ServicosDeTransferencia {
	
	@Autowired
	private KafkaTemplate<String, String> kafkaTemplate;
	
	@Autowired
	private ITransportadoraXYZServicos transportadoraXYZServicos;
	
	public void transfere(Pedido pedido) {
		try {
			Transferencia transferencia = transportadoraXYZServicos.transfer(pedido);
			if(transferencia != null) {
				String json = new ObjectMapper().writeValueAsString(transferencia);
		    	kafkaTemplate.send("pedidoTransferido", json);
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public List<Transferencia> getAll() {
		return transportadoraXYZServicos.getAll();
	}
	
	@KafkaListener(topics = "novoPedidoValidaTransferencia")
	public void recebeMensagem(ConsumerRecord<?, ?> consumerRecord) {
		try {
		    String mensagem = (String) consumerRecord.value();
			Pedido pedido = new ObjectMapper().readValue(mensagem, Pedido.class);
			if(pedido.isTemCuidadosEspeciais())
				transfere(pedido);
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	

}
