package com.puc.tcc.logistica.transferencia.webservices.impl;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Repository;

import com.puc.tcc.logistica.transferencia.dominio.Endereco;
import com.puc.tcc.logistica.transferencia.dominio.Pedido;
import com.puc.tcc.logistica.transferencia.dominio.Transferencia;
import com.puc.tcc.logistica.transferencia.webservices.ITransportadoraXYZServicos;
import com.puc.tcc.logistica.transferencia.webservices.correio.CorreioWebservice;
import com.puc.tcc.logistica.transferencia.webservices.correio.servico.EnderecoERP;

@Repository
public class TransportadoraXYZServicos implements ITransportadoraXYZServicos {

	@Autowired
	CorreioWebservice correioWebservice;
	
	public static List<Transferencia> simulacaoDados = new ArrayList<Transferencia>();
	
	@Override
	public Transferencia transfer(Pedido pedido) {
		try {
			Transferencia transf = new Transferencia();
			transf.setCodigoPedido(pedido.getId());
			transf.setData(new Date());
			transf.setDescricaoTrecho(descricaoDeRota(pedido.getEnderecoOrigem(), pedido.getEnderecoDestino()));
			float custo = (pedido.getLarguraCaixa()+pedido.getAlturaCaixa()+pedido.getProfundidadeCaixa())*pedido.getPeso();
			transf.setCusto(custo);
			simulacaoDados.add(transf);
			return transf;
		} catch (Exception e) {
			// TODO: handle exception
			e.printStackTrace();
		}
		return null;
	}
	
	@Override
	public List<Transferencia> getAll() {
		return simulacaoDados;
	}
	
	protected String descricaoDeRota(Endereco endOrigem, Endereco endDestino) throws Exception {
		
		EnderecoERP origem = correioWebservice.buscaCep(String.valueOf(endOrigem.getCep()));
		String descricaoOrigem = "De " + origem.getEnd()  + " " + endOrigem.getNumero() + ", " + origem.getCidade();

		EnderecoERP destino = correioWebservice.buscaCep(String.valueOf(endDestino.getCep()));
		String descricaoDestino = " para " + destino.getEnd() + " " + endDestino.getNumero() + ", " + destino.getCidade();
		return descricaoOrigem + descricaoDestino;
	}

}