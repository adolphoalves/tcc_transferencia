package com.puc.tcc.logistica.transferencia.visao.exceptions;

public class PedidoNotFoundException extends RuntimeException {

	private static final long serialVersionUID = 1L;

	public PedidoNotFoundException(Long id) {
		super("Não foi possível encontrar o pedido " + id);
	}
	
	

}
